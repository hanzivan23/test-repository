from datetime import datetime as dt

#format the date as "YYYY-MM-DD"
d_format = '%Y-%m-%d'

#Enter the specific date / User input
date_str1 = input("Enter the first date (YYY-MM-DD): ")

#Convert the entered date string into a datetime object
date1 = dt.strptime(date_str1, "%Y-%m-%d")

date_str2 = input("Enter the second date (YYY-MM-DD): ")

#Convert the entered date string into a datetime object
date2 = dt.strptime(date_str2, "%Y-%m-%d")


#calculate the number of days between days
num_days = abs((date1 - date2).days)

#Result
print("Number of days between the given dates: ", num_days, "days")