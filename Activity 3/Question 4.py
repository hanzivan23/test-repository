# Test Data
amt = 10000  # Principal amount
int = 3.5  # Interest rate
years = 7  # Number of years

# Calculate the future value using the function
future_value = amt *((1+(0.01*int)) ** years)

# Print the result
print("Future value:", round(future_value, 2))
