#Assign the pname and breed of the Dog
class Pet:
    def __init__(self, name, breed):
        self.name = name
        self.breed = breed

    def display_info(self):
        print(f"Name: {self.name}, Breed: {self.breed}")


class Dog(Pet):
    def __init__(self, name, breed):
        super().__init__(name, breed)
        self.species = "Dog"

    def bark(self):
        print("Arf Arf!")

#Assign the pname and breed of the Cat
class Cat(Pet):
    def __init__(self, name, breed):
        super().__init__(name, breed)
        self.species = "Cat"

    def meow(self):
        print("Meow meow!")


#test data
dog = Dog("Hamly", "German Shepherd")
cat = Cat("Bella", "Persian")

#result / output
dog.display_info()  
dog.bark()        

cat.display_info()  
cat.meow()       
