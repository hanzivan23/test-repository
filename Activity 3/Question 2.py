#User input
number = int(input("Enter a number: "))

#define to check if the number is odd
def odd(num):
    return num % 2 == 1

#check if the number is odd
if odd(number):
    print(f"The number {number} is Odd")
else:
    print(f"The number {number} is Even")

