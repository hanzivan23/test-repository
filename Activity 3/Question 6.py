from datetime import datetime as dt

date1 = dt(2022, 1, 1)
date2 = dt(2021, 1, 1)

difference = date1 - date2
print(difference)

def combine(value1, value2):
    # Check if the values are of the same type
    if type(value1) == type(value2):
        # If both are strings, concatenate them
        if isinstance(value1, str):
            return value1 + value2
        # If both are integers, multiply them
        elif isinstance(value1, int):
            return value1 * value2
        # If both are lists, concatenate them
        elif isinstance(value1, list):
            return value1 + value2
        # If both are dictionaries, merge them
        elif isinstance(value1, dict):
            combined_dict = value1.copy()
            combined_dict.update(value2)
            return combined_dict
        # If both are datetime objects, calculate the difference
        elif isinstance(value1, dt) and isinstance(value2, dt):
            return value1 - value2
    else:
        return "NOT VALID"

#create datetime objects 
date1 = dt(2022, 1, 1)
date2 = dt(2021, 1, 1)

#difference between two dates
difference = date1 - date2

difference = date1 - date2
# result
print(combine("Hello, ", "I'm Hanz")) 
print(combine(6, 3))
print(combine([1, 2, 3], [4, 5, 6]))  
print(combine(date1, date2))
