def change_first_char(input_string):
    # Get the first character of the input string
    first_char = input_string[0]

    # Start building the modified string with the first character
    mod_string = first_char

    # Loop through each character starting from the second one
    for char in input_string[1:]:
        # If the character matches the first character, replace it with "$"
        if char == first_char:
           mod_string += "$"
        # If the character is different from the first character, keep it unchanged
        else:
            mod_string += char

    return mod_string

string = 'restart'
result = change_first_char(string)
print("Result: ", "'" + result + "'")
