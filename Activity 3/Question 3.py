def near_thousand(number):
    return abs(number - 1000) <= 100 or abs(number - 2000) <= 100

# Test Data
test_numbers = [1000, 900, 800, 2200]

# Check each test number
for number in test_numbers:
    result = near_thousand(number)
    print(f"{number} = {result}")