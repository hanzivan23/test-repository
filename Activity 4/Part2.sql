-- Add a new record to the patients table
INSERT INTO patients (firstname, lastname, middlename, birthdate, gender, civil_status, address, email_address, contact_number)
VALUES ('James', 'Nebres', 'Pineda', '1999-12-25', 'Male', 'Single', '123 Esperanza St', 'james12@gmail.com', '0922-345-9876');
-- Update one record in the patients table
UPDATE patients
SET email_address = 'updated_email@gmail.com'
WHERE patientId = 1; -- Replace 1 with the ID of the record you want to update
-- Remove one record from the patients table
DELETE FROM patients
WHERE patientId = 2; -- Replace 2 with the ID of the record you want to delete
-- Display all records from the patients table and arrange by age.
SELECT *
FROM patients
ORDER BY birthdate;
-- Display columns from both Patients and Examinations tables.
SELECT p.*, e.exam_name, e.results, e.date_created
FROM patients p
JOIN examinations e ON p.patientId = e.patientId;
-- Count all records in the patients table.
SELECT COUNT(*) AS total_records
FROM patients;

