-- Create database named "inet"
CREATE DATABASE inet;


-- Use the "inet" database
USE inet;

-- Add table named "patients"
CREATE TABLE patients (
    patientId INT AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(50),
    lastname VARCHAR(50),
    middlename VARCHAR(50),
    birthdate DATE NOT NULL,
    gender ENUM('Male', 'Female', 'Other'),
    civil_status ENUM('Single', 'Married', 'Divorced', 'Widowed', 'Separated'),
    address VARCHAR(255),
    email_address VARCHAR(100),
    contact_number VARCHAR(20)
);

-- Add table named "examinations"
CREATE TABLE examinations (
    examId INT AUTO_INCREMENT PRIMARY KEY,
    patientId INT NOT NULL,
    exam_name VARCHAR(100),
    results TEXT,
    date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (patientId) REFERENCES patients(patientId)
);

-- Create unique index on Patients -> firstname, lastname and middlename
CREATE UNIQUE INDEX idx_name ON patients (firstname, lastname, middlename);
