def concatenate_elements(list):
    concatenated_string = '' 
    for char in list:  
        concatenated_string += char  
    return concatenated_string

# Example list
listelements = ["Hello", " ", "World", "!"]
result = concatenate_elements(listelements)
print("Concatenated string: ", result)